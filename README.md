A minimal site developed for [TEDxUniversityofMacedonia](https://www.tedxuniversityofmacedonia.com/el/).

It's goal is to be a central hub of information for all TEDxUniversityofMacedonia events.

Deployed at [http://info.tedxuniversityofmacedonia.com/](http://info.tedxuniversityofmacedonia.com/)

# How to

## Prerequisites

Nodejs > 8


## How to launch app or build for production

First clone the repository, then run `npm install` to install the project requirements.

Then you can launch the dev server by running `npm run develop` (really useful when adding new content)

To build the project for production run `npm run build` 

The resulting code will be in the public directory.

## Add new year

To add a new year event website you need to edit `src/pages/events.js`

Add a new entry in the array `eventsInfo` at the top e.g.

```javascript
const eventsInfo = [
  {
    year: 2018,
    theme: 'Chasm',
    url: 'https://www.tedxuniversityofmacedonia.com/',
  },
  {
    year: 2017,
    theme: 'Consciousnet',
    url: 'https://2017.tedxuniversityofmacedonia.com/',
  },
...
```

## Add talks 

You need to edit ` src/pages/talks.js `

You can add a new year in the `videoList` object e.g.to add 2018

```javascript
videoList: {
        2018: ['link1', 'link2', 'link3'],
        2017: ['https://www.youtube.com/embed/7si1CLY0-2w?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1',
        'https://www.youtube.com/embed/Q0NlUQ-DXbg?list=PLX0IQMHA-P-ObsnJM8nyh_V8fwIEiUJh1']
```

You can change the default active year by changing `activeYear`

Here for example you should edit it to 

```
activeYear: 2018
```
