import Typography from "typography";
import noriegaTheme from "typography-theme-noriega";


noriegaTheme.bodyGray = 0;
noriegaTheme.bodyGrayHue= 100;

noriegaTheme.overrideStyles = (PushSubscriptionOptions, styles) => ({
  '*': {
    color: 'white'
  }
})

const typography = new Typography(noriegaTheme);

export default typography;
