import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Header from '../components/header'
import Footer from "../components/footer"
import './index.css'

const Layout = ({ children, data }) => (
  <div
    style={{
      width: '100vw',
      height: '100vh',
      maxWidth: '100%',
    }}
    id="outer-container"
  >
    <Helmet
      title={data.site.siteMetadata.title}
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    />
    <Header siteTitle={data.site.siteMetadata.title} />
    <div
      id="page-wrap"
      style={{
        margin: '2rem auto',
        maxWidth: '1200px',
        padding: ' 0 1.0875rem 10rem 1.0875rem',
        flex: 1
      }}
    >
      {children()}
    </div>
    <Footer />
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
